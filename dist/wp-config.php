<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'iq3hwedfhgidsbf');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AX<-P6#=}DG TyK4tAWdLo>CY^#vtf[ggx!6&3Z<ms,jGHeo)(gNZ(G=XL-Rt7ve');
define('SECURE_AUTH_KEY',  '-9ZDP!uC[xPOBLz%:vLL)I/T1-SMk)FIZxZR&kgI*=W5e):J4:@sMchuq,C~h{tA');
define('LOGGED_IN_KEY',    '}ro*v(dY-RnYL=V[}.CuV9S&g%|Hj?o~-lyL4ziA.Q mkPLY%&hipO,J6ecJuD>v');
define('NONCE_KEY',        'XYxl;{I7MUKxJ%R*V.gwaN_v@poDm>$AtNjsR1lhp=Hxkf~e?X(2XHa%h&nX6h&9');
define('AUTH_SALT',        'Qf;4*$2Mr{F}~0%rs:8w}zy/FMQ;m;pGfGtkR&jE6u+B Z%<>^p*C9si[MbP!HZK');
define('SECURE_AUTH_SALT', '>p<Dgtv^m6M(fck^g<Wd%wyna&_+%.ikzZyFQlTGNtG`=X+eKLr}lQ*wM&c@|<hp');
define('LOGGED_IN_SALT',   'kthsc7Z@>_ZrR4[[i,)!%-h,r*ekr;=a/u[;jEC~f&HA8B1Do!uj&ErQThd^7PY+');
define('NONCE_SALT',       '0!H+-l7#854/3[*+no$.F.Ef%#8@/V_`t0dA_iq{ IMv2aPmnt2m4x?NtSnT7;m0');

/**#@-*/

define('FS_METHOD','direct');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
