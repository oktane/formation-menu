<?php get_header(); /* Template Name: Splash page */?>
    <section class="main front"
             <?php insert_bg('page_background_img', false, true);?>
             <?php if ( is_page(49) ) { echo 'redirect-after="10" redirect-url="'. home_url() .'"'; } ?>>

        <div class="front__wrapper">
            <img src="<?php bloginfo( 'template_directory' );?>/img/madd-logo--full.svg"
                 alt="Logo de la Maison des Arts Desjardins de Drummondville"
                 class="front__logo"/>
            <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post();
                $title_width = get_field('accueil__largeur_titre_svg');
                $title_width_mobile = get_field('accueil__largeur_titre_svg-mobile');
            ?>

            <div class="front__title-container">

                <?php if ( $title_width_mobile ) : ?>
                <style>
                    @media (max-width: 768px) {
                        .front__title-wrapper {
                            width: <?php echo $title_width_mobile.'% !important;';?>
                        }
                    }
                </style>
                <?php endif;?>

                <div class="front__title-wrapper" <?php if ( $title_width ) { echo 'style="width: '.$title_width.'%;"'; } ?>>
                    <?php if ( is_front_page() ) : ?>
                    <img class="front__title" src="<?php the_field( 'accueil__titre_svg' );?>" alt="Dégustation auditive"/>
                    <img class="front__title front__title--mobile" src="<?php the_field( 'accueil__titre_svg-mobile' );?>" alt="Dégustation auditive" />
                    <?php endif;?>

                    <h1 class="front__subtitle"><?php echo get_the_content();?></h1>

                    <a href="<?php the_field('accueil__link_to_poll');?>" class="big-button front__navbtn front__navbtn--mobile">Débuter</a>
                </div>
            </div>

            
            <?php if ( is_front_page() ) : ?>
            <a href="<?php the_field('accueil__link_to_poll');?>" class="big-button front__navbtn">Débuter</a>
            <?php endif;?>

            <?php endwhile;?>
            <?php endif;?>
        </div>
    </section>
<?php get_footer(); ?>