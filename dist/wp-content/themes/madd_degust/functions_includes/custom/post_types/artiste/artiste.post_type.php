<?php

// Register Custom Post Type
function artiste_post_type() {

	$labels = array(
		'name'                  => _x( 'Artistes', 'Post Type General Name', 'madd_degust' ),
		'singular_name'         => _x( 'Artiste', 'Post Type Singular Name', 'madd_degust' ),
		'menu_name'             => __( 'Artistes', 'madd_degust' ),
		'name_admin_bar'        => __( 'Artiste', 'madd_degust' ),
		'archives'              => __( 'Item Archives', 'madd_degust' ),
		'attributes'            => __( 'Item Attributes', 'madd_degust' ),
		'parent_item_colon'     => __( 'Parent Item:', 'madd_degust' ),
		'all_items'             => __( 'All Items', 'madd_degust' ),
		'add_new_item'          => __( 'Add New Item', 'madd_degust' ),
		'add_new'               => __( 'Ajouter un artiste', 'madd_degust' ),
		'new_item'              => __( 'Nouvel artiste', 'madd_degust' ),
		'edit_item'             => __( 'Modifier l\'artiste', 'madd_degust' ),
		'update_item'           => __( 'Mettre à jour l\'artiste', 'madd_degust' ),
		'view_item'             => __( 'Voir l\'artiste', 'madd_degust' ),
		'view_items'            => __( 'Voir les artistes', 'madd_degust' ),
		'search_items'          => __( 'Chercher un artiste', 'madd_degust' ),
		'not_found'             => __( 'Not found', 'madd_degust' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'madd_degust' ),
		'featured_image'        => __( 'Featured Image', 'madd_degust' ),
		'set_featured_image'    => __( 'Set featured image', 'madd_degust' ),
		'remove_featured_image' => __( 'Remove featured image', 'madd_degust' ),
		'use_featured_image'    => __( 'Use as featured image', 'madd_degust' ),
		'insert_into_item'      => __( 'Insert into item', 'madd_degust' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'madd_degust' ),
		'items_list'            => __( 'Liste des artistes', 'madd_degust' ),
		'items_list_navigation' => __( 'Items list navigation', 'madd_degust' ),
		'filter_items_list'     => __( 'Filter items list', 'madd_degust' ),
	);
	$rewrite = array(
		'slug'                  => 'artistes',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Artiste', 'madd_degust' ),
		'description'           => __( 'Artiste à venir à la maison des Arts', 'madd_degust' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'taxonomies'            => array(),
        'menu_icon'             => 'dashicons-admin-users',
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'rewrite'				=> $rewrite,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'artiste', $args );

}
add_action( 'init', 'artiste_post_type', 0 );