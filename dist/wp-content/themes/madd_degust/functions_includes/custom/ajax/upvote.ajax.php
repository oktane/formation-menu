<?php
function ajax_filter_setup() {
    add_action( 'wp_ajax_upvoting', 'ajax_upvote' );
    add_action( 'wp_ajax_nopriv_upvoting', 'ajax_upvote' );
}
add_action( 'after_setup_theme', 'ajax_filter_setup' );


function ajax_upvote() {
    if ( isset($_POST['data']) ) {
        session_start();

        $_SESSION["form_id"] = "";
        $_SESSION["artist_ids"] = "";
        $upvoted_artist_ids = $_POST['data'];
        $string_artiste = "";

        foreach ($upvoted_artist_ids as $id) {
            update_field('artiste__vote_count', get_field('artiste__vote_count', $id) + 1, $id);
            $string_artiste .=get_the_title($id).", ";
        }
        $_SESSION["form_id"] = $_POST['pageID'];
        $_SESSION["artist_ids"] = $string_artiste;
    }
    session_write_close();
    wp_die();
}


/*Field dynamiquely populated*/
add_filter( 'gform_pre_render_1', 'inject_form_and_choices' );
add_filter( 'gform_pre_validation_1', 'inject_form_and_choices' );
add_filter( 'gform_pre_submission_filter_1', 'inject_form_and_choices' );
add_filter( 'gform_admin_pre_render_1', 'inject_form_and_choices' );
function inject_form_and_choices( $form ) {
    @session_start();
    //print_r($_SESSION);
    if(isset($_SESSION['form_id']) && isset($_SESSION["artist_ids"])){
        foreach ( $form['fields'] as &$field ) {
            if($field->id == 16){//Titre de la page de vote
                $field->defaultValue =get_the_title($_SESSION['form_id']);
            }

            if ( $field->id == 17) {//Nom des artiste ayant été choisi
                $field->defaultValue =$_SESSION['artist_ids'];
            }

        }
    }

    return $form;
}
