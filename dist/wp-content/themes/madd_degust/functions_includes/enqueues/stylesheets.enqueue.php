<?php

function okt_enqueue_styles() {
    global $wp_styles;
    $themecsspath = get_stylesheet_directory() . '/style.css';

    wp_enqueue_style(
        'madd_degust_styles',
        get_stylesheet_directory_uri().'/style.css',
        array('fancybox_styles', 'jq_material_styles'),
        filemtime( get_stylesheet_directory() . '/style.css' )
    );

    wp_register_style(
        'fancybox_styles',
        get_stylesheet_directory_uri().'/css/vendors/jquery.fancybox.min.css',
        array(),
        ''
    );

    wp_register_style(
        'jq_material_styles',
        get_stylesheet_directory_uri().'/css/vendors/jquery-material-inputs.css',
        array(),
        ''
    );

    if ( is_page(23) ) {
        wp_enqueue_style('fancybox_styles');
    } else if ( is_page(41) ) {
        wp_enqueue_style('jq_material_styles');
    }
}
add_action('wp_enqueue_scripts', 'okt_enqueue_styles');