<?php

function insert_bg($field_or_url, $nofield = false, $print = false) {
    $style_attr = '';
    
    if ( !$nofield && get_field($field_or_url) ) {
        $style_attr = 'style="background-image: url(\''. get_field($field_or_url) .'\');"';
    } else if ( $nofield ) {
        $style_attr = 'style="background-image: url(\''. $field_or_url .'\');"';
    }

    if ( $print )
        echo $style_attr;
    else
        return $style_attr;
}