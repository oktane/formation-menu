<?php get_header();?>
    <section class="main front"
             <?php insert_bg('page_background_img', false, true);?>>

        <div class="front__wrapper">
            <img src="<?php bloginfo( 'template_directory' );?>/img/madd-logo--full.svg"
                 alt="Logo de la Maison des Arts Desjardins de Drummondville"
                 class="front__logo"/>

            <div class="front__title-container">

                <div class="front__title-wrapper">

                    <h1 class="front__subtitle">La page demandée est introuvable...</h1>

                    <a href="<?php echo get_home_url(); ?>" class="big-button">Retour à l'accueil</a>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>