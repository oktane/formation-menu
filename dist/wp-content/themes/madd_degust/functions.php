<?php

show_admin_bar( false );
// get the the role object
$role_object = get_role( 'editor' );
// add $cap capability to this role object
$role_object->add_cap( 'edit_theme_options' );

 /* HOOKS */
 include_once 'functions_includes/hooks/enable-svg.hook.php';


/* ENQUEUES */
include_once 'functions_includes/enqueues/stylesheets.enqueue.php';
include_once 'functions_includes/enqueues/scripts.enqueue.php';

/* ADMIN UI CUSTOMIZATION */
include_once 'functions_includes/admin_ui/menu.admin_ui.php';
include_once 'functions_includes/admin_ui/artsite-custom-colums.action.php';

/* DEBUG HELPERS */
include_once 'functions_includes/debug/functions.debug.php';

/* HELPERS */
include_once 'functions_includes/helpers/insert_bg.helper.php';

/* COMPONENTS */
include_once 'functions_includes/components/buttons.component.php';
include_once 'functions_includes/components/artiste_card.component.php';

/* MISC */
include_once 'functions_includes/custom/ajax/upvote.ajax.php';

/* CUSTOM POST TYPES */
include_once 'functions_includes/custom/post_types/artiste/artiste.post_type.php';

/* CUSTOM FIELDS */
include_once 'functions_includes/custom/fields/accueil.fields.php';
include_once 'functions_includes/custom/fields/artiste.fields.php';
include_once 'functions_includes/custom/fields/entetes.fields.php';
include_once 'functions_includes/custom/fields/sondages.fields.php';
include_once 'functions_includes/custom/fields/votes.fields.php';