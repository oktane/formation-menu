'use strict';
// ***********************
// GULP DEPENDENCIES
// ***********************
const gulp 		 	= require('gulp');
const gulpIf 		= require('gulp-if');
const inject 		= require('gulp-inject');
const copy 		 	= require('gulp-copy');
const browserSync   = require('browser-sync').create();
const sass 		 	= require('gulp-sass');
const rename       	= require('gulp-rename');
const autoprefixer 	= require('gulp-autoprefixer');
const sourcemaps   	= require('gulp-sourcemaps');
const concat       	= require('gulp-concat');
const g_util       	= require('gulp-util');
const watch        	= require('gulp-watch');
const notify       	= require('gulp-notify');
const ftp          	= require('vinyl-ftp');
const plumber      	= require('gulp-plumber');
const uglify 		= require('gulp-uglify');
const newer 		= require('gulp-newer');
const imagemin 	 	= require('gulp-imagemin');
const pngquant     	= require('imagemin-pngquant');
const series 		= require('stream-series');
const runSequence  	= require('gulp-run-sequence');
const babelify		= require('babelify');
const browserify	= require('browserify');
const _buffer 		= require('vinyl-buffer');
const source 		= require('vinyl-source-stream');


// ***********************
// PROJECT NAME
// ***********************
const app_name = 'madd_degust';

const IS_DEV = false;

function vinyl(dirGlob, dir, msgNoti) {
	const conn = ftp.create( {
		host:     ftp_host,
		user:     ftp_nomusager,
		password: ftp_motdepasse,
		port:     ftp_port,
		parallel: 10,
		reload:   true,
		log:      g_util.log
	});
	const globs = [
		'./dist' + dirGlob
	];
	return gulp.src( globs, { buffer: false } )
	.pipe( conn.newer( ftp_basedir + '/' + dir ) )
	.pipe( conn.dest( ftp_basedir + '/' + dir ) )
	.pipe(notify({ message: msgNoti, onLast: true }));
}


/*********************************************************************
**********************************************************************
*********                                                    *********
*********   SECONDARY TASKS TO BE CALLED RIGHT ON TERMINAL   *********
*********                                                    *********
**********************************************************************
****************************************************************/


// ***********************
// SASS
// ***********************

var   src_path  = './src/sass/';
const sass_main = 'style-injected.sass';
const dist_path = `./dist/wp-content/themes/${ app_name }`;

gulp.task('inject-sass', function() {

	const templateSass 	 = gulp.src(['src/sass/templates/**/*.sass'], {read: false});
	const helpersSass 	 = gulp.src(['src/sass/helpers/**/*.sass', '!src/sass/helpers/variables/**/*.+(sass|scss)'], {read: false});
	const baseSass 		 = gulp.src(['src/sass/base/**/*.+(sass|scss)'], {read: false});
	const componentsSass = gulp.src(['src/sass/components/**/*.sass'], {read: false});
	const layoutSass 	 = gulp.src(['src/sass/layouts/**/*.sass'], {read: false});
	const viewsSass 	 = gulp.src(['src/sass/views/**/*.sass'], {read: false});

    return gulp.src(['./src/sass/style.sass'])

	.pipe(inject(series(helpersSass, baseSass, componentsSass, templateSass, layoutSass, viewsSass), {
		relative: false,
		ignorePath: 'src/sass',
		addRootSlash: false,
		starttag: '/* inject:imports */',
		endtag: '/* endinject */',
		transform: function (filepath) {
			return '@import "' + filepath + '"';
		}
	}))
	.pipe(rename('style-injected.sass'))
    .pipe(gulp.dest('./src/sass'));
});

gulp.task('sass', ['inject-sass'],function() {
	return gulp.src(src_path + sass_main)
	.pipe(gulpIf(IS_DEV, sourcemaps.init()))
	.pipe(plumber({
		errorHandler: function (err) {
			console.log(err.messageFormatted);
			this.emit('end');
		}
	}))
	.pipe(sass({
		outputStyle: IS_DEV ? 'expanded' : 'compressed'
	}))
	.pipe(autoprefixer({
		browsers: [
			'Android >= 4',
			'Chrome >= 30',
			'Firefox >= 20',
			'Explorer >= 9',
			'iOS >= 6',
			'Safari >= 5']
		}))
	.pipe(rename('style.css'))
	.pipe(gulpIf(IS_DEV, sourcemaps.write()))
	.pipe( gulp.dest( dist_path ) )
    .pipe(browserSync.stream());
});
// ***********************
// STYLE UPLOAD
// ***********************
gulp.task( 'deploy-style',['sass'], function () {
	return vinyl('/style.css', app_name,  'Tâche : "deploy-style" Complété!');
});

// ***********************
// IMAGE OPTIMISATION
// ***********************
gulp.task('imagemin', function () {
	return gulp.src( ['src/img/**/*', '!./src/img/svg/*.svg'])
	.pipe(newer(`dist/wp-content/themes/${ app_name }/img`))
	.pipe(imagemin({
		optimizationLevel: 7, progressive: true, interlaced: true,
		use: [pngquant()],
		verbose: true
	}))
	.pipe(gulp.dest( `dist/wp-content/themes/${ app_name }/img` ))
	.pipe( notify( { message: 'Tâche : "imagemin" Complété!', onLast: true } ) );

});
// UPLOAD IMAGES OPTIMISÉ
gulp.task( 'deploy-images',['imagemin'], function () {
	return vinyl( '/img/**/*.{png,svg,jpg,jpeg,gif}', app_name + '/img' , 'Tâche : "deploy-image" Complété!');
});

//*****************************
//JS VENDORS
//*****************************/

gulp.task('babel_browserify', () => {
	const bundler = browserify({
		entries: './src/js/app.js'
	});

	bundler.transform(babelify.configure({
		presets:  ['es2015'],
		plugins:  ['transform-class-properties'],
		minified: !IS_DEV,
		compact:  !IS_DEV,
		comments: IS_DEV
	}));

	return bundler.bundle()
		.pipe(source('app.bundle.js'))
		.pipe(_buffer())
		// .pipe(uglify({
		// 	mangle: false
		// }).on('error', function(e){
		// 	console.log(e);
		// }))
		.pipe(gulp.dest(`dist/wp-content/themes/${ app_name }/js`));
});

gulp.task( 'vendorsjs', function() {
	return gulp.src( 'src/js/vendors/**/*.js' )
	.pipe( concat( 'vendors.js' ) )
	.pipe( gulp.dest( 'src/js' ) )
	.pipe( rename( {
		basename: 'vendors',
		suffix: '.min'
	}))
	.pipe(uglify().on('error', function(e){
		console.log(e);
	}))
	.pipe( gulp.dest( `dist/wp-content/themes/${ app_name }/js` ) )
	.pipe( notify( { message: 'Tâche : "vendorsJs" Complété!', onLast: true } ) )
});

// UPLOAD VENDOR JS
gulp.task( 'deploy-vendorsjs',['vendorsjs'], function () {
	return vinyl( '/js/vendors.min.js', app_name + '/js', 'Tâche : "deploy-vendorJS" Complété!');
});

//*****************************
//JS CUSTOMS
//*****************************/
gulp.task( 'customsjs', function() {
	return gulp.src( 'src/js/customs/**/*.js' )
	.pipe( concat( 'customs.js' ) )
	.pipe( gulp.dest( 'src/js' ) )
	.pipe( rename( {
		basename: 'customs',
		suffix: '.min'
	}))
	.pipe( uglify().on('error', function(e){
		console.log(e);
	}) )
	.pipe( gulp.dest( `dist/wp-content/themes/${ app_name }/js` ) )
	.pipe( notify( { message: 'Tâche : "vendorsJs" Complété!', onLast: true } ) )
});
// UPLOAD VENDOR JS
gulp.task( 'deploy-customsjs',['babel_browserify'], function () {
	return vinyl( '/js/*.js', app_name + '/js', 'Tâche : "deploy-customsJS" Complété!');
});

// ***********************
// PHP FILE COPY TO DIST
// ***********************

gulp.task('copy_root', function() {
	return gulp.src('./src/**/*.php')
	.pipe(newer(`./dist/wp-content/themes/${ app_name }`))
	.pipe(copy(`./dist/wp-content/themes/${ app_name }`, {
		prefix: 1
	}));
});

// ***********************
// PHP UPLOAD
// ***********************

gulp.task( 'deploy-generalphp',['copy_root'], function () {
	return vinyl('/**/*.php', app_name ,  'Tâche : "deploy-generalphp" Complété!');
});

/****************************************************************
*****************************************************************
*********                                               *********
*********   MAIN TASKS TO BE CALLED RIGHT ON TERMINAL   *********
*********                                               *********
*****************************************************************
****************************************************************/
gulp.task( 'default', function () {
    browserSync.init({
        proxy: 'localhost:8888',
        open: false
    });

	/* Watches SASS files to lauch 'sass' task */
	gulp.watch(['src/sass/**/*.+(sass|scss)'], ['sass']);
	gulp.watch(['src/**/*.php'], ['copy_root']);
	gulp.watch(['src/img/*.{png,gif,jpg,svg}'], ['imagemin']);
	gulp.watch(['src/js/**/*.js'], [ 'babel_browserify' ] );
	
    gulp.watch(['dist/**/*.+(php|js)']).on('change', browserSync.reload);
});

gulp.task( 'build', function () {
	runSequence('copy_root', ['sass', 'copy_root', 'imagemin']);
});
