export class Tools {
    static WP_getPageID() {
        const pageIDPrefix = 'page-id-';
        let pageID = document.body.className.substring(document.body.className.indexOf(pageIDPrefix));

        return parseInt(pageID.substring(pageIDPrefix.length, pageID.indexOf(' ')));
    }
}