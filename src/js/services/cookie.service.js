export class CookieService {
    static exists(cookieName = '') {
        return document.cookie.indexOf(cookieName) !== -1;
    }

    static set(name = 'custom_cookie', value = 'true', days = 1) {
        if ( !this.exists(name) ) {
            /* Setting expire date to half a day. (12h) */
            let _expireDate = new Date();
            _expireDate.setDate(_expireDate.getDate() + days);

            document.cookie = `${ name }=${ value }; expires=${ new Date(_expireDate).toUTCString() };`;
        }
    }
}