if ( !('values' in Object) ) {
    Object.values = function(obj) {
        const out = [];

        for (let key in obj) {
            out.push(obj[key]);
        }

        return out;
    }
}