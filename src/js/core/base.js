/*
    Those exports are made to avoid making over and over again
    a property inside classes like this.$window = jQuery(window);
 */
export const $window   = jQuery(window);
export const $body     = jQuery(document.body);
export const $htmlBody = jQuery('html, body');