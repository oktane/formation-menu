export { injectCtrlToElement } from './inject_ctrl_to_element';
export { bootstrap } from './bootstrap';
export { $window, $body, $htmlBody } from './base';