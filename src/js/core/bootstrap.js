/*
    This will instantiate every directive in a given Object and return an array of object containing
    the name of each directive and all of its instances.

    Note: every directive MUST contain an 'selector' STATIC PROPERTY which must represent the CSS selector
    of which elements should be affected by the directive.
*/
export function bootstrap(directives) {
    const directivesInstances = [];

    if ( directives.length ) {
        for (let i = 0; i < directives.length; i++) {
            let _curr = directives[i];

            if ( 'selector' in _curr && typeof _curr.selector === 'string' ) {
                if ( 'use' in _curr ) {
                    _curr.use.selector = _curr.selector;
                    _curr = _curr.use;
                }

                let isInArray = !directives.filter(dir => dir.name === _curr.name);
                let _instances = initClass(_curr.selector, _curr); // The init static method of each directives should ALWAYS return an array of its instances

                /* We check if there is already a directive named like the one
                we want to init. If there isn't, we create a new object. */
                if ( !isInArray.length && _instances.length ) {
                    directivesInstances.push({
                        name: _curr.name,
                        instances: _instances
                    });
                }
                /* If this directive already been instantiated, we just push
                new instances in its array of instances. */
                else if ( isInArray.length && _instances.length ) {
                    directivesInstances[directivesInstances.indexOf(isInArray[0])].instances.push(initClass(_curr.selector));
                }
                
            }
        }
    }
    jQuery(window).trigger('App.bootstrapped');
    return directivesInstances;
}

function initClass(selector, _Directive) {
    let _instances = [];
    const els = document.querySelectorAll(selector);

    for (let i = 0; i < els.length; i++) {
        let _inst = new _Directive(els[i]);
        _instances.push(_inst); // Push instance into returned array
    }

    return _instances;
}