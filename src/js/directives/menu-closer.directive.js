export class MenuCloserDirective {
    App;
    $element;
    static selector = '[menu-closer]';

    constructor(element) {
        this.$element = jQuery(element);

        this._bindEvents();
        this._onAppBootstrapped();
    }

    _bindEvents() {
        jQuery(window).on('App.bootstrapped', this._onAppBootstrapped.bind(this));
        this.$element.on('click', this._onHostClick.bind(this));
    }

    _onAppBootstrapped() {
        this.App = window.App;
    }

    _onHostClick(evt) {
        jQuery('.header--menu-opened').removeClass('header--menu-opened');
        setTimeout(MenuCloserDirective.handleSubMenus, 250);
    }

    static handleSubMenus() {
        const _menuItemHasChildrenInstances = MenuCloserDirective.findMenuItemHasChildren();

        for (let i = 0; i < _menuItemHasChildrenInstances.length; i++) {
            let _curr = _menuItemHasChildrenInstances[i];

            if ( _curr.isOpened )
                _curr.isOpened = false;
        }
    }

    static findMenuItemHasChildren() {
        return App.filter(dir => dir.name === 'MenuItemHasChildren')[0].instances;
    }
}