import { $window } from '../core';
import { TweenMax, Power2 } from 'gsap';
import 'gsap/ScrollToPlugin';

export class Autoscroll {
    elementShown = false;
    static selector = '[autoscroll]';

    constructor(element) {
        this.$element = jQuery(element);
        this.$target  = jQuery(this.getTargetSelector(element)); // Will be used by TweenLite to find the height of the target
        this.showPositions   = this.getShowPositions(element);

        this.speed  = element.getAttribute('scroll-speed') || 1; // Scroll speed in set in seconds.
        this.offset = parseFloat(element.getAttribute('scroll-offset')) || 0;

        // On init
        this.bindEvents();
        this.onScroll();
    }
    
    getTargetSelector(element) {
        const _href = element.getAttribute('href');
        
        if ( _href !== null ) {
            return document.querySelector(_href);
        } else {
            throw new Error('Autoscroll directive host element MUST have an \'href\' attribute.');
            return;
        }
    }

    getShowPositions(element) {
        let _pos   = element.getAttribute('show-positions');
        let output = {
            from: 0,
            to: document.body.clientHeight - window.innerHeight
        };
        
        if ( _pos !== null ) {
            const _posArr = _pos.replace(/ /g, '').split(',');

            if ( _posArr[0] ) output.from = _posArr[0];
            if ( _posArr[1] ) output.to   = _posArr[1];
        }

        return output;
    }

    bindEvents() {
        this.$element.on('click', this.onClick.bind(this));
        $window.on('scroll', this.onScroll.bind(this));
    }

    onClick(evt) {
        evt.preventDefault();

        TweenMax.to(window, 1, {
            scrollTo: this.$target.offset().top + this.offset,
            ease: Power2.easeInOut
        });
    }

    onScroll() {
        let wScroll = window.pageYOffset;

        if ( wScroll >= this.showPositions.from && wScroll <= this.showPositions.to && this.elementShown )
            this.showElement();
        else if ( wScroll > this.showPositions.to && !this.elementShown )
            this.hideElement();
    }

    hideElement() {
        this.elementShown = true;
        TweenMax.to(this.$element, 0.25, {
            alpha: 0,
            onComplete: () => {
                TweenMax.set(this.$element, {display: 'none'});
            }
        });
    }

    showElement() {
        this.elementShown = false;
        TweenMax.set(this.$element, {display: 'inline-block'});
        TweenMax.to(this.$element, 0.25, {
            alpha: 1
        });
    }
}