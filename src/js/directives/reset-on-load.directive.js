export class ResetOnLoad {
    static selector = 'form[reset-on-load], form.reset-on-load';

    constructor(element) { 
        this.element = element;

        // On init
        this.reset();
    }

    reset() {
        this.element.reset();
    }
}