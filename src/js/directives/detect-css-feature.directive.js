export class DetectCSSFeature {
    static selector = '[detect-css-feature]';

    constructor(element) {
        this.$element = jQuery(element);
        this.feature  = element.getAttribute('detect-css-feature');

        // On init
        this.checkDefaults();
        this.init();
    }

    checkDefaults() {
        if ( !this.feature )
            throw new Error('DetectCSSFeature directive host element MUST have a value.');
    }

    init() {
        if ( this.hasFeature() )
            this.$element.addClass(`browser-supports__${ this.feature }`);
        else
            this.$element.addClass(`browser-no-supports__${ this.feature }`);
    }

    hasFeature() {
        return this.feature in document.body.style;
    }
}