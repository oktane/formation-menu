export class SetCarretToStart {
    static selector = '[set-carret-to-start]';

    constructor(element) {
        this.$element = jQuery(element);

        // On init
        this.bindEvents();
    }

    bindEvents() {
        this.$element.on('focus click', this.changeCarretPosition.bind(this));
    }

    changeCarretPosition() {
        setTimeout(() => {
            SetCarretToStart.setCaretPosition(this.$element[0], 0);
        }, 100);
    }
    

    static setCaretPosition(elem, caretPos = 0) {
        const lastNumberIndex = getLastNumberIndex(elem.value);
        const index = lastNumberIndex === -1 ? 0 : lastNumberIndex;
        const isFocused = document.activeElement === elem;

        if ( elem !== null ) {
            if ( elem.createTextRange ) {
                const range = elem.createTextRange();
                range.move('character', index);
                range.select();
            } else {
                if( elem.selectionStart ) {
                    elem.setSelectionRange(index, index);
                    !isFocused && elem.focus();
                } else {
                    !isFocused && elem.focus();
                } 
            }
        }
    }
}

function getLastNumberIndex(str) {
    const acceptedChars  = '0123456789-() ';
    const forbiddenChars = '_';

    for (let i = 0; i < str.length; i++) {
        let _char = str.charAt(i);

        if ( _char === '_' ) {
            return i;
        } 
    }

    return -1;
}