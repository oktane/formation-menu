/*
    This directive has also been made mainly to work on Gravity Forms, which explains its weird selector.

    To use it, you have to give a specific value in Gravity Forms edition page of the form on the master
    checkbox.
    
    - The master checkbox is the element that will control if whether of not the slave element will
    be disabled.

    - The slave checkbox is the element on which you have to give a custom class of .disable-if which
    will activate the current directive. You also have to give it another class beginning with
    the prefix 'disabled-if__' followed by the value of the master checkbox.

    Ex.:

    * Master has value of 'my-master-value'

    * Slave's gform <li/> parent has a class of 'disabled-if disabled-if__my-master-value'
*/

export class DisabledIf {
    static selector = '.gfield.disabled-if';

    constructor(element) {
        this.$slaveParent = jQuery(element);
        this.$slave       = jQuery('input[type="checkbox"]', this.$slaveParent);
        this.$master      = jQuery(`.gfield.${ this.getMasterSelector() } input[type="checkbox"]`);

        if ( !this.$master.length )
            throw new Error('DisbledIf directive cannot find master input element. Check classes of both slave and master elements.');

        // On init
        this.bindEvents();
        this.applyTransition();
        this.onMasterChange();
    }
    
    /* Parsing the master value inside of the slave's class list. */
    getMasterSelector() {
        const _prefix = 'disabled-if__';
        let masterSelector = Array.prototype.filter.call(
            this.$slaveParent[0].classList,
            _class => _class.indexOf(_prefix) !== -1
        )[0];

        if ( masterSelector === undefined ) {
            throw new Error(
                'DisabledIf directive has to have a given master selector in order to watch its master element.' +
                'Ex.: class="disabled-if disabled-if__my-selector"'
            );
        }

        masterSelector = masterSelector.replace('disabled-if__', '');
        return masterSelector;
    }

    applyTransition() {
        this.$slaveParent.css({
            transition: 'all 0.1s linear'
        });
    }

    bindEvents() {
        this.$master.on('change', this.onMasterChange.bind(this));
    }

    onMasterChange() {
        const masterChecked = this.$master[0].checked;

        if ( masterChecked )
            this.enable();
        else
            this.disable();
    }

    disable() {
        this.$slaveParent.css({
            opacity: 0.5,
            pointerEvents: 'none'
        });
        
        if ( this.$slave[0].checked ) {
            this.$slave.trigger('click');
            this.$slave[0].checked = false;
            this.$slave.attr('disabled', 'true');
        }
    }

    enable() {
        this.$slaveParent.css({
            opacity: 1,
            pointerEvents: 'all'
        });
        this.$slave[0].removeAttribute('disabled');
    }
}