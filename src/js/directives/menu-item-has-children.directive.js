export const SUBOPEN_CLASS = 'sub-opened';

export class MenuItemHasChildren {
    $hostLink;
    _isOpenedValue = false;
    static selector = '.menu-item-has-children';

    get isOpened() {
        return this._isOpenedValue;
    }
    set isOpened(val) {
        if (val !== this._isOpenedValue) {
            this._isOpenedValue = val;

            if (val)
                this.open();
            else
                this.close();
        }
    }

    // ON INIT //
    constructor(element) {
        this.$element = jQuery(element);
        this._cacheDOM();
        this._bindEvents();
    }

    _cacheDOM() {
        this.$hostLink = jQuery('> a', this.$element);
        this.$mainParent = jQuery('#mainHeader');
    }

    _bindEvents() {
        this.$hostLink.on('click', this._onHostClick.bind(this));
    }

    _onHostClick(evt) {
        evt.preventDefault();
        this._toggleOpen();
    }

    _toggleOpen() {
        this.isOpened = !this.isOpened;
    }

    open() {
        this.$element.add(this.$mainParent).addClass(SUBOPEN_CLASS);
    }

    close() {
        this.$element.add(this.$mainParent).removeClass(SUBOPEN_CLASS);
    }
}