import { CookieService as cookie, Tools } from '../services';

export class AjaxSubmit {
    static selector = 'form[ajax-submit]';
    formSent = false; // To avoid multiple send.
    
    constructor(element) {
        // Elements
        this.$element = jQuery(element);
        this.$btn     = this.$element.attr('submit-btn') ?
                        jQuery(this.$element.attr('submit-btn'), this.$element) :
                        jQuery('[type="submit"]', this.$element);

        // Options
        this.wpAction    = this.$element.attr('wp-action') || '';
        this.redirectUrl = this.$element.attr('redirect-url') || location.href;
        this.getAll      = this.$element.attr('get-all') || '';

        // On init
        this.checkIfForm();
        this.bindEvents();
        this.$element[0].reset();
    }

    checkIfForm() {
        if ( this.$element[0].tagName.toLowerCase() !== 'form')
            throw new Error('Directive AjaxSubmit must be apply to element of type <form/> only!');
    }

    bindEvents() {
        this.$element.on('submit', this.onSubmit.bind(this));
    }

    onSubmit(evt) {
        evt.preventDefault();
        if ( this.formSent ) return;

        /* Extracting checkboxes values */
        let _values = evt.target.elements[this.getAll];
        _values = Array.prototype
                    .filter.call(_values, input => input.checked)
                    .map(input => input.value);
        
        this.btnOnLoad();
        jQuery.post(window[this.wpAction].ajax_url, {
            'action': this.wpAction,
            'data': _values,
            'pageID': Tools.WP_getPageID()
        })
        .success(() => {
            cookie.set('wp-upvote', 'true', 1);
            this.onSubmitSuccess();
        })
        .error(this.onSubmitError.bind(this));
    }

    onSubmitSuccess() {
        this.formSent = true;
        document.location.href = this.redirectUrl;
    }

    onSubmitError() {
        this.formSent = true;
        alert('Une erreur s\'est produite.');
        document.location.href = this.redirectUrl;
    }

    btnOnLoad() {
        this.$btn.text('Chargement...');
        this.$btn.attr('disabled', 'true');
    }
}