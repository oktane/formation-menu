/*
    This directive has been made mainly to work on Gravity Forms, which explains its weird selector.

    It will check every checkboxes from the same group if the bound element is checked and uncheck
    them if it's unchecked. Also, the box will check by itself if every other checkbox from the same group
    are checked and it will uncheck itself if every other checkbox from the same group are NOT checked.
*/

export class CheckAll {
    static selector = 'input[type="checkbox"][value="okt-check-all"]';

    constructor(element) {
        this.$element  = jQuery(element);
        this.gformID   = this.$element.attr('name').substring(0, this.$element.attr('name').indexOf('.'));
        this.$siblings = jQuery(`input[type="checkbox"][name^="${ this.gformID }"]`).not(this.$element);

        this.bindEvents();
    }

    bindEvents() {
        this.$element.on('change', this.onElementChange.bind(this));
        this.$siblings.on('change', this.onSiblingChange.bind(this));
    }

    onElementChange() {
        if ( this.$element[0].checked )
            this.checkAll();
        else
            this.uncheckAll();
    }

    checkAll() {
        this.$siblings.each((i, el) => {
            el.checked = true;
        });
    }

    uncheckAll() {
        this.$siblings.each((i, el) => {
            el.checked = false;
        });
    }

    hasUnchecked() {
        for (let i = 0; i < this.$siblings.length; i++) {
            if ( !this.$siblings.eq(i)[0].checked ) {
                return true;
            }
        }
        return false;
    }

    onSiblingChange() {
        if ( this.hasUnchecked() && this.$element[0].checked )
            this.$element[0].checked = false;
        else if ( !this.hasUnchecked() && !this.$element[0].checked )
            this.$element[0].checked = true;
    }
}