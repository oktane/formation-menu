export class LabelFor {
    isCheckbox = false; // Element is <input type="checkbox"/>
    trueText    = '';   // Text to apply on element on checkbox change to true
    falseText   = '';   // Text to apply on element on checkbox change to false
    trueClass   = '';   // Class to add is checkbox value is to true
    $parentElement;
    $titleElement;
    _parentValue;
    static selector = '[data-label-for]';

    constructor(element) {
        this.$element = jQuery(element);
        this.$elementParent = this.$element.parent();
        this.parentID = this.$element.attr('data-label-for');

        this.initText = this.$element.text();

        if ( this.$element.data('title-element') )
            this.$titleElement = jQuery(this.$element.data('title-element'), this.$element);
        if ( this.$element.data('true-class') )
            this.trueClass = this.$element.data('true-class');

        // On init
        this.cacheDOM();
        this.defineParentType();
        this.getAltValues();
        this.bindEvents();
    }

    get parentValue() {
        return this._parentValue;
    }
    set parentValue(val) {
        this._parentValue = val;
        this.toggleCheckedClass(val);
        this.toggleText(val);
    }

    cacheDOM() {
        this.$parentElement = jQuery(`#${ this.parentID }`);
    }

    defineParentType() {
        this.isCheckbox = this.$parentElement[0].type.toLowerCase() === 'checkbox';
        this.$parentElement.on('change', this.updateValue.bind(this));
    }

    getAltValues() {
        if ( !this.isCheckbox ) return;

        if ( this.$element.attr('data-true-text') )
            this.trueText = this.$element.attr('data-true-text');
        if ( this.$element.attr('data-false-text') )
            this.falseText = this.$element.attr('data-false-text');
    }

    bindEvents() {
        this.$element.on('click.datalabelfor', this.onElementClick.bind(this));
    }

    updateValue() {
        if ( this.isCheckbox )
            this.parentValue = this.$parentElement[0].checked;
        else
            this.parentValue = this.$parentElement.val();
    }

    toggleCheckedClass(val) {
        if ( !this.isCheckbox ) return;

        if ( val ) {
            this.$element.addClass(`checked${ ' ' + this.trueClass }`);
            this.$elementParent.addClass(`checked${ ' ' + this.trueClass }`);
        } else {
            this.$element.removeClass(`checked${ ' ' + this.trueClass }`);
            this.$elementParent.removeClass(`checked${ ' ' + this.trueClass }`);
        }
    }

    toggleText(val) {
        const $textContainer = this.$titleElement ? this.$titleElement : this.$element;

        if ( val && this.trueText.length )
            $textContainer.text(this.trueText);
        else if ( !val && this.falseText.length )
            $textContainer.text(this.falseText);
    }

    onElementClick() {
        this.$parentElement.click();
        this.updateValue();
    }
}