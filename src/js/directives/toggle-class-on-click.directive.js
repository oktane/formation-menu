export class ToogleClassOnClick {
    static selector = '[toggle-class-on-click]';
    $remoteElementSelector;
    className;

    constructor(element) {
        this.$element = jQuery(element);

        this._getAttr();
        this._bindEvents();
    }

    _getAttr() {
        this.$remoteElementSelector = this.$element.attr('remote-element') || null;
        this.className = this.$element.attr('toggle-class-on-click') || '';
    }

    _bindEvents() {
        this.$element.on('click', this._onHostClick.bind(this));
    }

    _onHostClick(evt) {
        const _$elToChange = this.$remoteElementSelector ? jQuery(this.$remoteElementSelector) : this.$element;

        _$elToChange.toggleClass(this.className);
    }
}