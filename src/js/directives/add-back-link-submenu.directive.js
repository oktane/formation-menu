import { SUBOPEN_CLASS } from './menu-item-has-children.directive';
import { MenuCloserDirective } from './menu-closer.directive';

export class AddBackLink {
    static selector = '.sub-menu';

    constructor(element) {
        this.$element = jQuery(element);
        this.activeClassName = element.getAttribute('active-class') || '';
        this.subMenuOpenedClassName = 'sub-opened';
        this.langAttribute = document.documentElement.lang;
        this.createBackLink();
        this.addBackLink();
        this.bindEvents();
    }

    bindEvents() {
        this.$element.on('click', this.closeSubMenu.bind(this));
    }

    createBackLink() {
        const isFrench = this.langAttribute.indexOf('fr') != -1;
        const backWord = isFrench ? 'Retour' : 'Back';
        const template = `
            <li class="back-link">
                <a href="#">${ backWord }</a>
            </li>
        `;
        this.$backLink = jQuery(template);
    }

    addBackLink() {
        this.$element.prepend(this.$backLink);
    }

    closeSubMenu() {
        MenuCloserDirective.handleSubMenus();
    }

}