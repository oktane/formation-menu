export class EmptyHref {
    static selector = 'a[href="#"], a[href=""], a:not([href])';

    constructor(element) {
        this.element = element;

        // On init
        this.bindEvents();
    }

    bindEvents() {
        this.element.addEventListener('click', this.onEmptyClick.bind(this));
    }

    onEmptyClick(e) {
        e.preventDefault();
        console.log('empty clicked');
    }
}