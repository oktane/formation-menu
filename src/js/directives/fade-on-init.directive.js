import { TweenLite, Power2 } from 'gsap';

export class FadeOnInit {
    static selector = '[fade-on-init]';

    constructor(element) {
        this.element     = element;
        this.fadeInTime  = element.getAttribute('fade-in-time') || 1;
        this.fadeOutTime = element.getAttribute('fade-out-time') || 1;
        this.fadeDelay   = parseFloat(element.getAttribute('fade-delay')) || 0;

        this.fadeOut();
    }

    fadeIn(time = this.fadeInTime, delay = this.fadeDelay) {
        return new Promise(resolve => {
            TweenLite.to(this.element, time, {
                delay,
                alpha: 1,
                onComplete: () => {
                    this.element.style.display = 'block';
                    resolve();
                }
            });
        });
    }

    fadeOut(time = this.fadeOutTime, delay = this.fadeDelay) {
        return new Promise(resolve => {
            TweenLite.to(this.element, time, {
                delay,
                alpha: 0,
                onComplete: () => {
                    this.element.style.display = 'none';
                    resolve();
                }
            });
        });
    }
}