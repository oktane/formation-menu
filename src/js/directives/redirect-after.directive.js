export class RedirectAfter {
    static selector = '[redirect-after]';

    constructor(element) {
        this.redirectDelay = element.getAttribute('redirect-after') || 30; // redirectDelay is in SECONDS
        this.redirectURL   = element.getAttribute('redirect-url');          // redirectURL is MANDATORY
        
        // On init
        this.checkErrors();
        this.startCounting();
        console.log(this.redirectDelay);
    }

    checkErrors() {
        if ( !this.redirectURL )
            throw new Error('RedirectAfter directive host element MUST have a redirect-url attribute.');
    }

    startCounting() {
        setTimeout(
            this.redirect.bind(this),
            parseFloat(this.redirectDelay) * 1000
        );
    }

    redirect() {
        location.href = this.redirectURL;
    }
}