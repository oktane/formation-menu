import { CookieService as cookie } from '../services';

export class RedirectIfCookie {
    static selector = '[redirect-if-cookie]';

    constructor(element) {
        this.cookieName  = element.getAttribute('redirect-if-cookie');
        this.redirectURL = this.setRedirectURL(element);
        
        // On Init
        this.checkIfCookie();
    }

    setRedirectURL(element) {
        const _attr = element.getAttribute('redirect-url');


        if ( _attr )
            return _attr;
        else if ( document.referrer !== undefined )
            return document.referrer;
        else
            return location.host;
    }

    checkIfCookie() {
        if ( cookie.exists(this.cookieName) ) {
            location.href = this.redirectURL;
        }
    }
}