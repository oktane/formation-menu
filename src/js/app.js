import './core/polyfills/object_values.polyfill';
import { bootstrap } from './core';
import * as Directives from './directives';


// Adding a custom selector to the 'check-all' directive.
const directiveClasses = Object.values(Directives).map(curr => {
    if ( curr.name === 'CheckAll' ) {
        return {
            use: curr,
            selector: 'input[type="checkbox"][value="Tous"]'
        };
    }

    if ( curr.name === 'SetCarretToStart' ) {
        return {
            use: curr,
            selector: '.ginput_container_phone input'
        }
    }

    return curr;
});

// Bootstrapping directives
console.time('Directives bootstrap done in');
const App = bootstrap(directiveClasses);
console.timeEnd('Directives bootstrap done in');
console.log(App);

window.App = App;


// Instantiating jQuery plugins
if ( 'materialInputs' in jQuery.fn ) {
    jQuery('input[type="text"], input[type="email"], textarea')
    .materialInputs({
        scaleRatio: 0.7,
        autoCenter: false,
        raiseTo: '14px'
    });
}