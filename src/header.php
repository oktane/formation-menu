<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="theme-color" content="#231F20" />
    <?php include_once 'templates/favicons.php';?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); if ( is_front_page() ) { echo 'detect-css-feature="mix-blend-mode"'; } ?>>

    <?php include_once 'svg_defs.php';?>
    <div class="overlay overlay--main"
         menu-closer>
    </div>

    <div class="curtain" fade-on-init fade-out-time="0.4" fade-delay="<?php if (is_page(23)) { echo 0.75; } else { echo 0.4; } ?>">
        <img src="<?php bloginfo( 'template_directory' );?>/img/madd-logo--single.svg"
             alt="Logo de la Maison des Arts Desjardins de Drummondville"
             class="curtain__logo"/>
    </div>

    <header class="header" id="mainHeader">
        <div class="header__wrapper">
            <div class="header__logo-container">
                <img src="<?php bloginfo('template_directory');?>/img/madd-logo--single.svg" alt="Logo de la Maison des Arts Desjardins de Drummondville" class="header__logo">
            </div>

            <!-- Burger -->
            <div class="header__burger-container">
                <div class="header__burger burger"
                     toggle-class-on-click="header--menu-opened"
                     remote-element="#mainHeader">

                    <div class="burger__bar burger__bar--top"></div>
                    <div class="burger__bar burger__bar--mid"></div>
                    <div class="burger__bar burger__bar--btm"></div>
                </div>
            </div>

            <div class="header__menu-container">
                <div class="header__menu-wrapper">
                    <?php wp_nav_menu('menu-principal');?>
                </div>
            </div>
        </div>
    </header>