<?php

// Discipline
function set_discipline_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Disciplines', 'Taxonomy General Name', 'madd_degust' ),
		'singular_name'              => _x( 'Discipline', 'Taxonomy Singular Name', 'madd_degust' ),
		'menu_name'                  => __( 'Discipline', 'madd_degust' ),
		'all_items'                  => __( 'Toutes les disciplines', 'madd_degust' ),
		'parent_item'                => __( 'Disciplines parent', 'madd_degust' ),
		'parent_item_colon'          => __( 'Disciplines parent:', 'madd_degust' ),
		'new_item_name'              => __( 'Nouvelle discipline', 'madd_degust' ),
		'add_new_item'               => __( 'Ajouter une discipline', 'madd_degust' ),
		'edit_item'                  => __( 'Modifier la discipline', 'madd_degust' ),
		'update_item'                => __( 'Mettre à jour la discipline', 'madd_degust' ),
		'view_item'                  => __( 'Voir la discipline', 'madd_degust' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'madd_degust' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'madd_degust' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'madd_degust' ),
		'popular_items'              => __( 'Disciplines populaires', 'madd_degust' ),
		'search_items'               => __( 'Search Items', 'madd_degust' ),
		'not_found'                  => __( 'Not Found', 'madd_degust' ),
		'no_terms'                   => __( 'No items', 'madd_degust' ),
		'items_list'                 => __( 'Items list', 'madd_degust' ),
		'items_list_navigation'      => __( 'Items list navigation', 'madd_degust' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'discipline', array( 'artiste' ), $args );

}
add_action( 'init', 'set_discipline_taxonomy', 0 );