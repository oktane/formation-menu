<?php

function artiste_card($post_obj, $attr = '', $btn_attr = '') {
    $show_title = get_field('artiste__show-title', $post_obj->ID);
?>

    <div class="card" <?php if ( $attr != '' ) { echo $attr; } ?>>
        <div class="card__liked">
            <svg class="icon icon-like card__liked-icon">
                <use xlink:href="#icon-like"></use>
            </svg>
        </div>

        <div class="card__wrapper">
            <div class="card__pic" style="background-image: url('<?php echo get_the_post_thumbnail_url( $post_obj->ID );?>');"></div>
            <div class="card__body">
                <h3 class="card__title"><?php echo $post_obj->post_title;?></h3>
                <?php if ( $show_title ) : ?>
                <h5 class="card__subtitle"><?php echo $show_title;?></h5>
                <?php endif;?>

                <?php if ( get_field('artiste__url_extrait', $post_obj->ID) ) : $type = get_field('artiste__type_extrait', $post_obj->ID);?>
                <a href="#"
                   data-fancybox
                   data-src="<?php the_field('artiste__url_extrait', $post_obj->ID);?>"
                   data-width="1000"
                   data-height="600">
                    <?php arrow_button('Extrait '.$type, 'card__callto', '', $type); ?>
                </a>
                <?php endif;?>
            </div>
        </div>
        <?php bordered_button('aimer l\'artiste', 'card__likebtn', $btn_attr);?>
    </div>

<?php }