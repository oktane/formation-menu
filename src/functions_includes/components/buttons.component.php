<?php

function bordered_button($label = 'ok', $class = '', $attr = '') {?>
    <button type="button" class="bordered_button <?php echo $class;?>" <?php if ( $attr !== '' ) { echo $attr; }?>>
        <span class="bordered_button__label"><?php echo $label;?></span>
    </button>
<?php }

function arrow_button($label = 'ok', $class = '', $attr = '', $type = 'video') {?>
    <button type="button" class="arrow_button <?php echo $class;?>" <?php if ( $attr !== '' ) { echo $attr; }?>>
        <img src="<?php bloginfo('template_directory');?>/img/btn_<?php echo $type;?>_round.svg" alt="" class="arrow_button__icon"/>
        <span class="arrow_button__title"><?php echo $label;?></span>
    </button>
<?php }