<?php

function custom_admin_ui() {
    // Disable admin bar
    add_filter('show_admin_bar', '__return_false');
    // Advanced menu editin
    add_theme_support( 'menus' );
    // Featured image
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'custom_admin_ui' );