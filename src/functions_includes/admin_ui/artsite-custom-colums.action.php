<?php
/* This will add a column in the artiste listing to allow the admin to sort artists by vote count. */

function artiste_columns($columns) {
	$columns = array_merge($columns, array(
		'artiste__vote_count' => 'Votes'
	));
	return $columns;
}
add_filter('manage_edit-artiste_columns', 'artiste_columns');

function my_custom_columns($column) {
	global $post;
	if($column == 'artiste__vote_count') {
		echo get_field('artiste__vote_count', $post->ID);
	}
}
add_action('manage_artiste_posts_custom_column', 'my_custom_columns');

function sortable_artiste_custom_columns( $columns ) {
    $columns['artiste__vote_count'] = 'vote_count';
 
    return $columns;
}
add_filter( 'manage_edit-artiste_sortable_columns', 'sortable_artiste_custom_columns' );

function vote_count_orderby( $query ) {
    if( ! is_admin() )
        return;
 
    $orderby = $query->get( 'orderby');
 
    if( 'vote_count' == $orderby ) {
        $query->set('meta_key','artiste__vote_count');
        $query->set('orderby','meta_value_num');
    }
}
add_action( 'pre_get_posts', 'vote_count_orderby' );