<?php

function okt_enqueue_scripts() {
    $scripts_path = get_stylesheet_directory_uri().'/js/app.bundle.js';

    /* Direct enqueue */
    wp_enqueue_script(
        'polyfills',
        'https://cdn.polyfill.io/v2/polyfill.min.js',
        array(),
        '',
        false
    );

    /* Direct enqueue */
    wp_enqueue_script(
        'madd_script_bundle',
        get_stylesheet_directory_uri().'/js/app.bundle.js',
        array('jquery', 'jquery-fancybox', 'jquery-material-inputs'),
        filemtime(get_stylesheet_directory().'/js/app.bundle.js'),
        true
    );
    wp_localize_script( 'madd_script_bundle', 'upvoting', array( 'ajax_url' => admin_url( 'admin-ajax.php' )));

    /* Registers */
    wp_register_script(
        'jquery-material-inputs',
        get_stylesheet_directory_uri().'/js/vendors/jquery-material-inputs.bundle.js',
        array('jquery'),
        '',
        true
    );

    wp_register_script(
        'jquery-fancybox',
        get_stylesheet_directory_uri().'/js/vendors/jquery.fancybox.min.js',
        array('jquery'),
        '',
        true
    );

    if ( is_page( 23 ) ) {
        wp_enqueue_script( 'jquery-fancybox' );
    } else if ( is_page(41) ) {
        wp_enqueue_script( 'jquery-material-inputs' );
    }
}
add_action('wp_enqueue_scripts', 'okt_enqueue_scripts');

