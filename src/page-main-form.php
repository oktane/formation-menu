<?php get_header(); /* Template name: Formulaire principal */ ?>
    <section class="main main--content main-form">
        <div class="main__wrapper main-form__wrapper">
            <?php get_template_part( 'templates/page', 'title.template' )?>

            <?php if ( have_posts() ) : ?>
            <div class="main__post-content main-form__post-content">
                <?php while ( have_posts() ) : the_post(); the_content();?>
                
                <?php endwhile;?>
            </div>
            <?php endif;?>
        </div>
    </section>

    <a href="#field_1_12"
       class="autoscroll main-form__autoscroll"
       autoscroll
       show-positions="0, 75"
       scroll-offset="-20">

        <div class="autoscroll__wrapper">
            <svg class="icon icon-arrow autoscroll__icon">
                <use xlink:href="#icon-arrow"></use>
            </svg>
            <span class="autoscroll__title">Descendre</span>
        </div>
    </a>
<?php get_footer(); ?>