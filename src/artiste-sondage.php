<?php get_header(); /* Template name: Sondage Artistes */?>
    <section class="main main--content artlist">
        <div class="main__wrapper artlist__wrapper">
            <?php get_template_part( 'templates/page', 'title.template' )?>
            
            <form name="vote_form"
                  class="artlist__form"
                  ajax-submit
                  reset-on-load
                  wp-action="upvoting"
                  redirect-url="<?php the_permalink(41);?>"
                  get-all="artistes[]"
                  submit-btn=".artlist__submit">
                  
                <ul class="artlist__card-list">
                    <?php
                        $artist_args = array(
                            'post_type' => 'artiste',
                            'posts_per_page' => 6
                        );

                        $artist_query = get_field('sondage__artists');

                        foreach ($artist_query as $artist) :
                            $input_ID = 'artist__'.$artist->ID;
                        ?>
                        <li class="artlist__card-item">
                            <input type="checkbox" id="<?php echo $input_ID;?>" class="artlist__checkbox" name="artistes[]" value="<?php echo $artist->ID;?>" tabindex="-1"/>
                            <?php artiste_card($artist, 'artlist__card', 'data-label-for="'.$input_ID.'" data-true-text="Vous aimez" data-false-text="Aimer l\'artiste" data-title-element=".bordered_button__label" data-true-class="liked"');?>
                        </li>
                            

                    <?php endforeach;?>
                </ul>

                <button type="submit" class="big-button artlist__submit">Continuer</button>
            </form>
        </div>
    </section>
<?php get_footer();?>